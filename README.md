# VPS AutoScriptVPN

### Installation
```
sudo su
wget -N https://gitlab.com/hugocollier/vpsroot/raw/main/root.sh && bash root.sh
```

```
apt update && apt upgrade -y && update-grub && sleep 2 && reboot
```

```
wget https://gitlab.com/hugocollier/ssh-ovpn/-/raw/main/setup && chmod +x setup && ./setup
```

### Download Link Configs OpenVPN
```
http://ipvps:85
```

### Additional Info
Recommended OS: Debian 9 & 10 x64 bit

### Finally
Credit To : https://github.com/rvpn/AutoScriptDB-1 (Orignal Base Script)  
Credit To : https://github.com/johndesu090/AutoScriptDeb8 (Orignal menu Script)  
Credit To : https://github.com/hassanmhd99/AutoScriptVPN  
